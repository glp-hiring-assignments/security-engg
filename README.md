APPLICATION DETAILS:

Shared with you is the implementation of a small application which enables you to:

a. Get yourself registered on the Register page.

b. Login through a registered account on the Login page.

c. See your details including Email, Password, First Name and Last Name on the User Details page.

 AIM:

AIM1:
1. Create a cloud machine to get the application deployed.
2. Deploy the application following easy steps mentioned below. If the deployment is successful you will be able to see the homepage with 3 options as mentioned above in 'Application Details' on http://<deployment_public_ip or domain>:8080
3. Get yourself registered on the application.
4. Find all the registered users' email, password, firstname and lastname without referring to the code or database directly.
5. Capture the screenshot of the results from STEP 4
6. Share the screenshot.
7. Share the machine link/URL details for us to verify the results.

 

AIM2:

We are using HttpSecurity in our implementation to authenticate the user in the application. There are few vulnerabilities associated with this specific application due to the same.

1. Find out all those vulnerabilities.

2. Suggestions to address them.

 

AIM3:

Bar Raiser:

Try to exploit a few of the vulnerabilities found in AIM2 and share the screenshot.

SYSTEM REQUIREMENTS:

1. Machine/Computer/Cloud Machine with at least 1 GB RAM, 5 GB HDD/SDD

2. Internet Connectivity

3. Git installed with details as git version 2.25.1 or later

4. Docker installed with details as Docker version 20.10.8 or later

5. Docker-compose installed with details as docker-compose version 1.29.2 or later

 DEPLOYMENT STEPS: 

1. Make sure all the mentioned system requirements are fulfilled in the machine you are deploying the code.

2. Clone the code:

    git clone https://greendeep@bitbucket.org/glp-hiring-assignments/security-engg.git

3. Go to the folder: cd security-engg

4. Build code and complete the deployment. Dont worry, we have simplified the steps for you. Just execute the mentioned command and it will take care of all the steps. Command to execute: docker-compose up --build --remove-orphans

 DELIVERABLES:

1. Screen shot of the page listing all the registered users.

2. Deployed application link/URL details.

3. Go through the code base and find all the existing implementation level vulnerabilities in the system. Share the detailed report on the same.